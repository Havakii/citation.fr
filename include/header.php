<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/public/style.css">
    <script src="https://kit.fontawesome.com/c637e730d5.js" crossorigin="anonymous"></script>
</head>
<body>
    <header id="myHeader">
        <div id="brand">
            <img src="/public/assets/logo.png" alt="" width="50">
            <a href="/public/index.php">Citations</a>
        </div>
        <nav>
            <ul>
                <li><a href="/public/index.php">Homepage</a></li>
                <li><a href="/public/citation.php">Citations</a></li>
                <li><a href="/public/auteur.php">Auteurs</a></li>
            </ul>
        </nav>
        <div id="hamburger-icon" onclick="toggleMobileMenu(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
            <ul class="mobile-menu">
                <li><a href="/public/index.php">Homepage</a></li>
                <li><a href="/public/citation.php">Citations</a></li>
                <li><a href="/public/auteur.php">Auteurs</a></li>
            </ul>
        </div>
    </header>
