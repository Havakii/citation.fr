<?php require '../connection.php' ?>
<?php include '../include/header.php' ?>
<main>
    <div class="aut-cit">
        <?php
            if (isset($_GET['id'])) {
                $id = (int) $_GET['id'];
                $res = $conn->query("SELECT * FROM auteur WHERE id_auteur = " . $id);
                $row = $res->fetch_assoc();
            if (isset($row['first_name']) && isset($row['last_name'])) {?>

                <h3><?php echo $row['first_name'] . ' ' . $row['last_name'];?></h3>
            <?php 
                $sql = ("SELECT * FROM citation
                WHERE id_auteur = " . $id);
                $result = $conn->query($sql); 
                foreach ($result as $citation){ ?>

                <p><?php echo $citation['citation_content'];?></p>

            <?php } ?>
        <?php } ?>
        <?php } ?>
    </div>
</main>

<?php include '../include/footer.php' ?>