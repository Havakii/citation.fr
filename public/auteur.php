<?php require '../connection.php' ?>
<?php include '../include/header.php' ?>

<main>
    <div class="container-auteur">
        <?php
            $request = "SELECT * FROM auteur";
            $result_aut = $conn->query($request);?>
        <?php foreach ($result_aut as $auteur){?>
            <a href="auteur_citation.php?id=<?php echo $auteur['id_auteur'];?>"><p><?php echo $auteur['first_name'] . ' ' . $auteur['last_name'];?></p></a>
        <?php } ?>
        </div>
</main>
<?php include '../include/footer.php' ?>