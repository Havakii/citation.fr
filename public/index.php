
<?php require '../connection.php' ?>
<?php include '../include/header.php' ?>

<main>
    <div class="quote-container">
    <?php
        $sql_cit = "SELECT citation.citation_content, auteur.first_name, auteur.last_name
        FROM citation, auteur
        WHERE citation.id_auteur = auteur.id_auteur
        ORDER BY id_citation DESC LIMIT 1";
        $result_cit = $conn->query($sql_cit);   
        foreach ($result_cit as $citation){ ?>
            <h1><?php echo $citation['citation_content'];?></h1>
        <?php } ?>
    <?php 
        $sql_aut = "SELECT citation.citation_content, auteur.first_name, auteur.last_name
        FROM citation, auteur
        WHERE citation.id_auteur = auteur.id_auteur
        ORDER BY id_citation DESC LIMIT 1";
        $result_aut = $conn->query($sql_aut);   
        foreach ($result_aut as $auteur){ ?>
            <h2>- <?php echo $auteur['first_name'] . ' ' . $auteur['last_name'];?> -</h2>
        <?php } ?>
        </div>
</main>

<?php include '../include/footer.php' ?>