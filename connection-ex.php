<?php
// Errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Create connection
$servername = 'yourServer';
$username = 'yourUserName';
$password = 'yourPassword';
$database = 'yourDataBase';


$conn = new mysqli($servername, $username, $password, $database);

// Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
// echo "Connected successfully";

?>