<?php require '../connection.php' ?>
<?php include '../include/header.php' ?>
<main>
    <div class="main_container">
        <h2>Auteurs</h2>
        <div class="list_container">
            <div class="button-container">
                <a href="add.php"><button type="button">Ajouter un auteur</button></a>
            </div>
                <table>
                    <tr>
                        <th>Auteurs</th>
                        <th>Actions</th>
                    </tr>
                    <?php      
                        $sql = "SELECT * FROM auteur";
                        $result = $conn->query($sql); 
                        foreach ($result as $auteur){ ?>
                    <tr>
                        <td>
                            <?php echo $auteur['first_name'] . ' ' . $auteur['last_name'];?>
                        </td>
                        <td>   
                            <a href="edit.php?id=<?php echo $auteur['id_auteur'];?>">Modifier</a><span>|</span>
                            <a href ="delete_aut.php?id=<?php echo $auteur['id_auteur'];?>">Supprimer</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
        </div>
    </div>  
</main>

<?php include '../include/footer.php' ?>