<?php require '../connection.php' ?>
<?php include '../include/header.php' ?>
<main>
    <div class="main_container">
    <h2>Auteurs</h2>
    <div class="list_container">
        
        <div class="button-container">
            <a href="add.php"><button type="button">Ajouter une citation</button></a>
        </div>
            <table>
            <tr>
                <th>Citations</th>
                <th>Actions</th>
            </tr>
            <?php      
                $sql = "SELECT id_citation, citation.id_auteur, citation.citation_content, auteur.first_name, auteur.last_name
                FROM citation, auteur
                WHERE citation.id_auteur = auteur.id_auteur";
                $result = $conn->query($sql); 
                foreach ($result as $citation){ ?>
            
            <tr>
                <td>
                <?php echo $citation['citation_content'];?>
                </td>
                <td>   
                <a href="edit.php?id=<?php echo $citation['id_citation'];?>">Modifier</a><span>|</span>
                <a href ="delete.php?id=<?php echo $citation['id_citation'];?>">Supprimer</a>
                </td>
            </tr>
                <?php } ?>
            </table>
    </div>
    </div>  
</main>

<?php include '../include/footer.php' ?>