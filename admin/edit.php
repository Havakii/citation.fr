<?php require '../connection.php' ?>
<?php include '../include/header.php' ?>
<main>
<div class="form-container">
        <h3>Modifier un auteur</h3>
    <?php
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $res = $conn->query("SELECT * FROM auteur WHERE id_auteur = " . $id);
            $row = $res->fetch_assoc();
        if (isset($row['first_name']) && isset($row['last_name'])) {?>
        <div class="form-auteur">
            <form method="post">
                <label>Nom</label>
                    <input type = "text" value="<?php echo $row['last_name']; ?>" name="lastName"></input>
                <label>Prénom</label>
                    <input type = "text" value="<?php echo $row['first_name']; ?>" name="firstName"></input>
                    <button type="submit" name="submit" value="submit">Modifier</button>
            </form>
        </div>
        <?php } ?>
        <?php } ?>
        <?php
            if (isset($_POST['lastName']) && isset($_POST['firstName'])) {
                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $sql = 'UPDATE auteur SET first_name="' . $firstName . '", last_name="' . $lastName . '" WHERE id_auteur = ' . $id;
                $result = $conn->query($sql);
                if ($result) {
                    header("Location:crud_auteurs.php");
                } else {
                    echo '<p class="error">Une erreur est survenue</p>';
                }
            } ?>
        <div class="form-aut-cit">
        <?php
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $res = $conn->query("SELECT * FROM citation WHERE id_citation = " . $id);
            $row = $res->fetch_assoc();
        if (isset($row['citation_content'])) {?>
            <form method="post">
                <h3>Modifier une citation</h3>
                <div class="cit">
                <input value="<?php echo $row['citation_content']; ?>"name="citation"></input>
                </div>
                <button type="submit" name="submit" value="submit">Modifier</button>
            </form>

        </div>
        <?php } ?>
        <?php } ?>
        <?php
            if (isset($_POST['citation'])) {
                $citation = addslashes($_POST['citation']);
                $sql = 'UPDATE citation SET citation_content="' . $citation . '" WHERE id_citation = ' . $id;
                $result = $conn->query($sql);
                if ($result) {
                    header("Location:crud_citations.php");
                } else {
                    echo '<p class="error">Une erreur est survenue</p>';
                }
            } ?>
    </div>
</main>
<?php include '../include/footer.php' ?>