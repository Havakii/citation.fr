<?php require '../connection.php' ?>
<?php include '../include/header.php' ?>
<main>
    <div class="form-container">
        <h3>Ajouter un auteur</h3>
        <div class="form-auteur">
        
            <form method="post">
                    <label>Nom</label>
                    <input type = "text" name="lastName"></input>
                    <label>Prénom</label>
                    <input type = "text" name="firstName"></input>
                    <button type="submit" name="submit" value="submit">Enregistrer</button>
            </form>
        </div>
        <?php
        if (isset($_POST['lastName']) && isset($_POST['firstName'])) {
            $lastName = $_POST['lastName'];
            $firstName = $_POST['firstName'];
            $sql = 'INSERT INTO auteur (first_name, last_name) VALUES ("' . $firstName . '", "' . $lastName . '")';
            echo $sql;
            $result = $conn->query($sql);
            if ($result) {
                header("Location:crud_auteurs.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
    ?>
        <div class="form-aut-cit">
            <form method="post">
                <h3>Attribuer une citation à un auteur</h3>
                <?php
                    $request = "SELECT * FROM auteur";
                    $result_aut = $conn->query($request);?>
                <select name ="auteurs">
                    <option hidden>Auteurs</option>
                        <?php foreach ($result_aut as $aut_list){?>
                    <option value = "<?php echo $aut_list['id_auteur'];?>" >
                        <?php echo $aut_list['first_name'];?>
                        <?php echo $aut_list['last_name'];?>
                    </option>
                        <?php } ?>
                </select>
                <div class="cit">
                <?php
        if (isset($_POST['submit']) ) {
            $citation = addslashes($_POST['citation']);
            $auteurId = $_POST['auteurs'];
            $sql = "INSERT INTO citation ( citation_content, id_auteur) VALUES ('$citation', '$auteurId')";
            echo $sql;
            $result = $conn->query($sql);
            if ($result) {
                header("Location:crud_citations.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
    ?>

<?php
        if (isset($_POST['id_auteur'])) {
        $auteurId = $_POST['id_auteur'];
        $sql = 'INSERT INTO citation (id_auteur) VALUES ("' . ((INT)$auteurId) . '"';
        $result = $conn->query($sql);
        if ($result) {
            header("Location:crud_auteurs.php");
        } else {
            echo '<p class="error">Une erreur est survenue</p>';
            }
        }
        ?>
                <input name="citation"></input>
                </div>
                <button type="submit" name="submit" value="submit">Enregistrer</button>
         
            </form>
        </div>
    </div>
</main>

<?php include '../include/footer.php' ?>